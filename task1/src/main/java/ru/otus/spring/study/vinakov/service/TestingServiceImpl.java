package ru.otus.spring.study.vinakov.service;

import ru.otus.spring.study.vinakov.entity.Question;
import ru.otus.spring.study.vinakov.exception.QuestionSourceParseException;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.*;

public class TestingServiceImpl implements TestingService {

    private final QuestionService questionService;
    private final PrintStream printStream;
    private final InputStream inputStream;

    public TestingServiceImpl(QuestionService questionService, PrintStream printStream, InputStream inputStream) {
        this.questionService = questionService;
        this.printStream = printStream;
        this.inputStream = inputStream;
    }

    @Override
    public boolean doTest(int maxErrors) {
        int errors = 0;
        Scanner scanner = new Scanner(inputStream);
        printStream.println("Hello! What is your name? ");
        String name = scanner.nextLine();
        printStream.println("Ok, " + name + ", let's begin testing");
        try {
            List<Question> questions = questionService.getAllQuestions();
            if (questions.size() == 0) {
                printStream.println("There are no questions, exam will be performed later. You have some more time to prepare :)");
            }
            for (Question question : questions) {
                printStream.println(question.getQuestion());
                Map<Integer, String> answers = new HashMap<>();
                int i = 1;
                List<String> variants = questionService.getVariants(question);
                Collections.shuffle(variants);
                for (String answer : variants) {
                    answers.put(i, answer);
                    printStream.println(String.format("%d. %s", i, answer));
                    i++;
                }
                printStream.println("Enter your answer (number): ");
                Integer index = scanner.nextInt();
                while (!answers.containsKey(index)) {
                    printStream.print("Invalid answer. Please, choose one of printed answers: ");
                    index = scanner.nextInt();
                }
                if (!questionService.isRightAnswer(question, answers.get(index))) {
                    errors++;
                }
            }
            StringBuilder result = new StringBuilder();
            if (errors <= maxErrors) {
                result.append("Congratulations! You finished test successfully.");
            } else {
                result.append("Unfortunately, you failed test.");
            }
            result.append(String.format("Your result: %d of %d", questions.size() - errors, questions.size()));
            printStream.println(result.toString());
            return errors <= maxErrors;
        } catch (QuestionSourceParseException e) {
            printStream.println("Sorry, there are some problems while preparing test. Please, check the file and try again later");
            return false;
        }
    }
}

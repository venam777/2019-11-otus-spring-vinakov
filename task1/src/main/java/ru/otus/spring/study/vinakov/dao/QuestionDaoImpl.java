package ru.otus.spring.study.vinakov.dao;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.MappingStrategy;
import ru.otus.spring.study.vinakov.entity.Question;
import ru.otus.spring.study.vinakov.exception.QuestionSourceParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QuestionDaoImpl implements QuestionDao {

    private final String fileName;
    private final char separator;

    private static final Logger logger = Logger.getLogger(QuestionDaoImpl.class.getName());

    public QuestionDaoImpl(String fileName, char separator) {
        this.fileName = fileName;
        this.separator = separator;
    }

    public List<Question> findAll() throws QuestionSourceParseException {
        InputStream inputStream = getClass().getResourceAsStream(fileName);
        try (InputStreamReader reader = new InputStreamReader(inputStream)) {
            MappingStrategy<Question> ms = new HeaderColumnNameMappingStrategy<>();
            ms.setType(Question.class);
            CsvToBean<Question> cb = new CsvToBeanBuilder<Question>(reader)
                    .withType(Question.class)
                    .withMappingStrategy(ms)
                    .withSeparator(separator)
                    .build();
            return cb.parse();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error while closing csv file reader!", e);
            throw new QuestionSourceParseException("Error while closing question file", e);
            //return Collections.emptyList();
        }
    }

}

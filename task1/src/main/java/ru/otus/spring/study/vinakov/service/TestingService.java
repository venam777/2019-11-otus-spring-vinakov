package ru.otus.spring.study.vinakov.service;

public interface TestingService {

    boolean doTest(int maxErrors);

}

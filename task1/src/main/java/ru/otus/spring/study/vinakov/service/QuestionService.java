package ru.otus.spring.study.vinakov.service;

import ru.otus.spring.study.vinakov.entity.Question;
import ru.otus.spring.study.vinakov.exception.QuestionSourceParseException;

import java.util.List;

public interface QuestionService {

    List<Question> getAllQuestions() throws QuestionSourceParseException;

    /**
     * Возвращает true, если ответ answer является правильным ответом на вопрос question
     * @param question вопрос
     * @param answer ответ (именно ответ, не вариант A\B\C\D)
     * @return true, если ответ правильный
     */
    boolean isRightAnswer(Question question, String answer);

    String getAnswer(Question question);

    List<String> getVariants(Question question);

}

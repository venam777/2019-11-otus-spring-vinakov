package ru.otus.spring.study.vinakov.exception;

public class QuestionSourceParseException extends Throwable {

    public QuestionSourceParseException() {
    }

    public QuestionSourceParseException(String message) {
        super(message);
    }

    public QuestionSourceParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public QuestionSourceParseException(Throwable cause) {
        super(cause);
    }

}

package ru.otus.spring.study.vinakov;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.otus.spring.study.vinakov.service.TestingService;


public class Main {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("/spring-context.xml");
        TestingService testingService = applicationContext.getBean(TestingService.class);
        testingService.doTest(0);
    }

}

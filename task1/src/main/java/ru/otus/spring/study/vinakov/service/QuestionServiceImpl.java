package ru.otus.spring.study.vinakov.service;

import ru.otus.spring.study.vinakov.dao.QuestionDao;
import ru.otus.spring.study.vinakov.entity.Question;
import ru.otus.spring.study.vinakov.exception.QuestionSourceParseException;

import java.util.Arrays;
import java.util.List;

public class QuestionServiceImpl implements QuestionService {

    private final QuestionDao questionDao;

    public QuestionServiceImpl(QuestionDao questionDao) {
        this.questionDao = questionDao;
    }

    @Override
    public List<Question> getAllQuestions() throws QuestionSourceParseException {
        return questionDao.findAll();
    }

    @Override
    public boolean isRightAnswer(Question question, String answer) {
        String rightAnswer = getAnswer(question);
        return rightAnswer.equalsIgnoreCase(answer);
    }

    @Override
    public String getAnswer(Question question) {
        switch (question.getAnswer().toUpperCase()) {
            case "A":
                return question.getAnswerA();
            case "B":
                return question.getAnswerB();
            case "C":
                return question.getAnswerC();
            case "D":
                return question.getAnswerD();
            default:
                return "UNKNOWN";
        }
    }

    @Override
    public List<String> getVariants(Question question) {
        return Arrays.asList(question.getAnswerA(), question.getAnswerB(), question.getAnswerC(), question.getAnswerD());
    }
}

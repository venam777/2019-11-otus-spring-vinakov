package ru.otus.spring.study.vinakov.dao;

import ru.otus.spring.study.vinakov.entity.Question;
import ru.otus.spring.study.vinakov.exception.QuestionSourceParseException;

import java.util.List;

public interface QuestionDao {

    List<Question> findAll() throws QuestionSourceParseException;

}

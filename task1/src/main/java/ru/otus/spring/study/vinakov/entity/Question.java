package ru.otus.spring.study.vinakov.entity;

import com.opencsv.bean.CsvBindByName;

public class Question {

    @CsvBindByName(column = "Question", required = true)
    private String question;
    @CsvBindByName(column = "A", required = true)
    private String answerA;
    @CsvBindByName(column = "B", required = true)
    private String answerB;
    @CsvBindByName(column = "C", required = true)
    private String answerC;
    @CsvBindByName(column = "D", required = true)
    private String answerD;
    @CsvBindByName(column = "Answer", required = true)
    private String answer;

    public Question() {}

    public Question(String question, String answerA, String answerB, String answerC, String answerD, String answer) {
        this.question = question;
        this.answerA = answerA;
        this.answerB = answerB;
        this.answerC = answerC;
        this.answerD = answerD;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswerA() {
        return answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public String getAnswer() {
        return answer;
    }

}

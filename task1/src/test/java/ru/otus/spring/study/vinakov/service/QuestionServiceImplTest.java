package ru.otus.spring.study.vinakov.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.otus.spring.study.vinakov.dao.QuestionDao;
import ru.otus.spring.study.vinakov.entity.Question;
import ru.otus.spring.study.vinakov.exception.QuestionSourceParseException;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Тестирование сервиса по работе с ответами на вопросы")
class QuestionServiceImplTest {

    private QuestionService questionService;

    private static ApplicationContext applicationContext;

    @BeforeAll
    static void initContext() {
        applicationContext = new ClassPathXmlApplicationContext("/spring-context.xml");
    }

    @BeforeEach
    void initService() {
        this.questionService = new QuestionServiceImpl(Mockito.mock(QuestionDao.class));
    }

    @Test
    @DisplayName("проверка чтения вопросов из файла")
    void testFindQuestions() throws QuestionSourceParseException {
        QuestionService questionService = applicationContext.getBean(QuestionService.class);
        List<Question> questions = questionService.getAllQuestions();
        assertNotNull(questions);
        assertTrue(questions.size() > 0);
    }

    @Test
    @DisplayName("проверка, является ли ответ на вопрос правильным")
    void testIsRightAnswer() {
        Question question = new Question("Some question", "answer1", "answer2", "answer3", "answer4", "B");
        assertTrue(questionService.isRightAnswer(question, "answer2"));
    }

    @Test
    @DisplayName("проврека получения правильного ответа на вопрос")
    void testGetAnswer() {
        Question question = new Question("Some question", "answer1", "answer2", "answer3", "answer4", "A");
        assertEquals("answer1", questionService.getAnswer(question));
    }
}